<?php include 'header.php'; ?>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Main Sims
                </div>

            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="./" method="post">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Text</label>
                            <input type="text" class="form-control" placeholder="Enter text">
                            <span class="help-block"> A block of help text. </span>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Email Address</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-envelope"></i>
                                </span>
                                <input type="email" name="email" class="form-control" placeholder="Email Address">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Password</label>
                            <div class="input-group">
                                <input type="password" class="form-control" placeholder="Password">
                                <span class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Password</label>
                            <div class="input-group">
                                <input type="file" class="form-control">
                                <span class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Left Icon</label>
                            <div class="input-icon">
                                <i class="fa fa-bell-o"></i>
                                <input type="text" class="form-control" placeholder="Left icon">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Right Icon</label>
                            <div class="input-icon right">
                                <i class="fa fa-microphone"></i>
                                <input type="text" class="form-control" placeholder="Right icon">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Input With Spinner</label>
                            <input type="password" class="form-control spinner" placeholder="Password">
                        </div>
                        

                    </div>
                    <div class="form-actions">
                        <div class="btn-set pull-left">
                            <button type="submit" name="ok" class="btn green">Submit</button>
                        </div>

                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
    </div>
</div>





<br>

<br>





<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Buttons</span>
                    <br> <br> <br> <br>
                    <br>
                </div>
                <div class="tools"> </div>
            </div>



            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Office</th>
                        <th>Age</th>
                        <th>Start date</th>
                        <th>Salary</th>

                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Office</th>
                        <th>Age</th>
                        <th>Start date</th>
                        <th>Salary</th>
                    </tr>
                </tfoot>
                <tbody>
                    <tr>
                        <td>Tiger Nixon</td>
                        <td>System Architect</td>
                        <td><span class="label label-sm label-warning"> Suspended </span></td>
                        <td>61</td>
                        <td>2011/04/25</td>
                        <td>$320,800</td>
                    </tr>

                </tbody>
            </table>






            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
</div>

</div>
</div>



<?php include 'footer.php'; ?>
