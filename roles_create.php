<?php ob_start(); ?>
<?php require_once './pdo_connection.php' ?>
<?php include 'header.php'; ?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="index.html">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Authentication</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>create Roles</span>
        </li>
    </ul>
</div>

<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Add Role
                </div>
            </div>

            <div class="portlet-body form">
                <?php 
                if(isset($_GET['status']))
                if($_GET['status'] == 'suc')
                {?>
                <div class="alert alert-success" role="alert">
                    data successfully saved!
                </div>
                <?php } else { ?>
                <div class="alert alert-danger" role="alert">
                    there is a problem with saving data!
                </div>
                <?php } ?>
                <!-- BEGIN FORM-->
                <form action="" method="post">

                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Add Roles *
                            </label>
                            <input type="text" name="role_name" class="form-control" placeholder="Enter name" required>
                            <br>
                           
                                <label>Area</label>
                              <br>
                                    <label class="mt-checkbox mt-checkbox-outline"> view areas
                                        <input style="float: left;" type="checkbox" value="1" name="test">
                                        <span></span>
                                    </label>
                                    <label class="mt-checkbox mt-checkbox-outline"> add areas
                                        <input type="checkbox" value="1" name="test">
                                        <span></span>
                                    </label>
                                    <label class="mt-checkbox mt-checkbox-outline"> edit areas
                                        <input type="checkbox" value="1" name="test">
                                        <span></span>
                                    </label>
                                    <label class="mt-checkbox mt-checkbox-outline"> delete areas
                                        <input type="checkbox" value="1" name="test">
                                        <span></span>
                                    </label>
                                <hr>
                                <label>Branch</label>
                              <br>
                                    <label class="mt-checkbox mt-checkbox-outline"> view branches
                                        <input type="checkbox" value="1" name="test">
                                        <span></span>
                                    </label>
                                    <label class="mt-checkbox mt-checkbox-outline">add branches
                                        <input type="checkbox" value="1" name="test">
                                        <span></span>
                                    </label>
                                    <label class="mt-checkbox mt-checkbox-outline">edit branches
                                        <input type="checkbox" value="1" name="test">
                                        <span></span>
                                    </label>
                                    <label class="mt-checkbox mt-checkbox-outline">delete branches
                                        <input type="checkbox" value="1" name="test">
                                        <span></span>
                                    </label>
                         
                                <hr>
                                
                                
                            

                        </div>

                    </div>

                    <div class="form-actions">
                        <div class="btn-set pull-left">
                            <button type="submit" name="insert" class="btn green">Add</button>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
    </div>
</div>

</div>
</div>
<?php include 'footer.php'; ?>
<?php
if(isset($_POST['insert']))
{
    if (empty($_POST['role_name'])) {
        header("Location:  roles_create?status=failed");

}

  $sql = "INSERT INTO `roles`(name,permission) VALUES (?,?)";
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(1, $_POST['role_name'], PDO::PARAM_STR);
  $stmt->bindValue(2, 'non', PDO::PARAM_STR);

  if($stmt->execute())
  {
    header("Location: roles_create?status=suc");
  }
  else
  {
    header("Location:  roles_create?status=failed");
  }
}


?>