<?php require_once 'pdo_connection.php'; ?>
<?php require_once 'header.php'; ?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="index.html">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Lookup Tables</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Departments</span>
        </li>
    </ul>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Departments
                    </span>

                </div>
                <div class="tools">
                    <button type="button" onclick="location.href='./departments_create'"
                        class="btn red mt-ladda-btn ladda-button btn-circle" data-style="slide-right">
                        <span class="ladda-label">New Departments</span>
                        <span class="ladda-spinner"></span></button>
                </div>
            </div>

            <br><br><br><br><br>

            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr>
                        <th style="width: 50%">Name</th>
                        <th style="width: 30%">Users</th>
                        <th style="width: 10%">Edit</th>
                        <th style="width: 10%">Delete</th>

                    </tr>
                </thead>

                <tbody>
                    <?php
                     $sql = "SELECT * FROM `departments` GROUP by dep_name";
                     $stmt = $conn->prepare($sql);         
                     $stmt->execute();
                       while ($row = $stmt->fetch())
                       {
                       
                    ?>
                    <tr>
                        <td><?php echo $row[1]; ?></td>
                        <td><?php echo $row[1]; ?></td>
                        <td>
                            <button type="button"
                                onclick="location.href='./departments_edit?id=<?php echo $row[0]; ?>'"
                                class="btn btn-info mt-ladda-btn ladda-button btn-circle" data-style="zoom-in">
                                <span class="ladda-label">Edit</span>
                            </button>
                        </td>
                        <td>
                            <button type="button" onclick="location.href='./delete_action?id=<?php echo $row[0]; ?>&table=departments&location=departments'"
                                class="btn btn-danger mt-ladda-btn ladda-button btn-circle" data-style="zoom-out">
                                <span class="ladda-label">Delete</span>
                                </button>
                        </td>
                    </tr>

                    <?php } ?>
                </tbody>
            </table>






            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
</div>

</div>
</div>



<?php require_once 'footer.php'; ?>