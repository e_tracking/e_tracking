<?php ob_start(); ?>
<?php require_once 'pdo_connection.php'; ?>
<?php
use Phppot\DataSource;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
require_once ('./vendor/autoload.php');
?>
<?php require_once 'header.php'; ?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="index.html">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Porchase</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Import Purchase Order File</span>
        </li>
    </ul>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Import Purchase Order File
                </div>

            </div>

            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <?php 
                if(isset($_GET['status']))
                if($_GET['status'] == 'suc')
                {?>
                <div class="alert alert-success" role="alert">
                    data successfully saved!
                </div>
                <?php } else { ?>
                <div class="alert alert-danger" role="alert">
                    there is a problem with saving data!
                </div>
                <?php } ?>

                <form action="" method="post" enctype="multipart/form-data">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">File Name</label>
                            <input type="text" name="filename" class="form-control" placeholder="File Name" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Purchase Date</label>
                            <input type="date" name="p_date" value="<?php echo date("Y-m-d") ;?>" class="form-control"
                                required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Supplying Number</label>
                            <input type="text" name="s_number" class="form-control" placeholder="Supplying Number"
                                required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Supplying Date</label>
                            <input type="date" name="s_date" value="<?php echo date("Y-m-d") ;?>" class="form-control"
                                required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">ERP Number</label>
                            <input type="text" name="erp" class="form-control" placeholder="ERP Number" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">City</label>
                            <select class="form-control" name="city">
                                <option selected>Select...</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Sim Types</label>
                            <select class="form-control" name="sim_types">
                                <option selected disabled>Select...</option>
                                <?php
                     $sql = "SELECT * FROM `sim_types` GROUP by sim_name";
                     $stmt = $conn->prepare($sql);         
                     $stmt->execute();
                   
                       while ($row = $stmt->fetch())
                       {  ?>
                                <option value="<?php echo $row[1];?>"><?php echo $row[1];?></option>

                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Quantity</label>
                            <input type="text" name="quantity" class="form-control" placeholder="Quantity" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Notes</label>
                            <textarea class="form-control" name="note" rows="4" cols="50"
                                placeholder="Notes"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label">File</label>
                            <input type="file" name="file"
                                accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                                class="form-control" required>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="btn-set pull-left">
                            <button type="submit" name="import" class="btn green">Import</button>
                        </div>

                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<?php include 'footer.php'; ?>
<?php
if(isset($_POST["import"]))
{
$filename = $_POST['filename'];
$p_date = $_POST['p_date'];
$s_number = $_POST['s_number'];
$s_date = $_POST['s_date'];
$erp = $_POST['erp'];
$city = $_POST['city'];
$sim_types = $_POST['sim_types'];
$quantity = $_POST['quantity'];
$note = $_POST['note'];

$actual_file_name = $_FILES["file"]["name"];
$temp = explode(".", $_FILES["file"]["name"]);
$newfilename = round(microtime(true)) . '.' . end($temp);
move_uploaded_file($_FILES["file"]["tmp_name"], "uploads/" . $newfilename);

$Reader = new Xlsx();

$spreadSheet = $Reader->load("uploads/" . $newfilename);
$excelSheet = $spreadSheet->getActiveSheet();
$spreadSheetAry = $excelSheet->toArray();
$sheetCount = count($spreadSheetAry);
if($sheetCount != $quantity)
{
    header("Location: import_purchase.php?status=failed");
    die();
}

$sql = "INSERT INTO `purchase_to_mainstore`(`file_name`, `purchase_date`, `supplying_number`, `supplying_date`, `ERP_number`, `city`, `sim_types`, `quantity`, `note`, `from_store`, `to_store`, `type`, `file_path`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
$stmt = $conn->prepare($sql);
$stmt->bindValue(1,$filename , PDO::PARAM_STR);
$stmt->bindValue(2,$p_date  , PDO::PARAM_STR);
$stmt->bindValue(3, $s_number, PDO::PARAM_STR);
$stmt->bindValue(4, $s_date, PDO::PARAM_STR);
$stmt->bindValue(5,$erp , PDO::PARAM_STR);
$stmt->bindValue(6,$city , PDO::PARAM_STR);
$stmt->bindValue(7,$sim_types , PDO::PARAM_STR);
$stmt->bindValue(8,$quantity , PDO::PARAM_STR);
$stmt->bindValue(9,$note , PDO::PARAM_STR);

$stmt->bindValue(10, 'Virtual Store' , PDO::PARAM_STR);
$stmt->bindValue(11,'Main Store	' , PDO::PARAM_STR);
$stmt->bindValue(12,'Purchase' , PDO::PARAM_STR);

$stmt->bindValue(13,"uploads/" . $newfilename, PDO::PARAM_STR);
$check = 0;
if($stmt->execute())
{
    $check = 1;
  //header("Location: import_purchase.php?status=suc");
}
else
{
    $check = 0;
 // header("Location: import_purchase.php?status=failed");
}

$sql = "";
for ($i = 0; $i < $sheetCount; $i++)
{
    $data = $spreadSheetAry[$i][0];
    $sql = $sql."INSERT INTO `nymbers_imported_mainstore`(`sim_no`, `file_name`, `store`) VALUES ('$data','$newfilename','main store')".";";
}
//echo $sql;
$stmt = $conn->prepare($sql);
if($stmt->execute())
{
    $check = 2;
}
else
{
    $check = 0;
}

if($check == 2)
{
  header("Location: import_purchase?status=suc");

}
else
{
  header("Location: import_purchase?status=failed");

}

}
?>