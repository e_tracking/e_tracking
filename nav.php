 <div class="page-sidebar-wrapper">
     <div class="page-sidebar navbar-collapse collapse">
         <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
             <li class="nav-item start ">
                 <a href="javascript:;" class="nav-link nav-toggle">
                     <i class="icon-home"></i>
                     <span class="title">Dashboard</span>
                     <span class="arrow"></span>
                 </a>
                 <ul class="sub-menu">
                     <li class="nav-item start ">
                         <a href="./" class="nav-link ">
                             <i class="icon-bar-chart"></i>
                             <span class="title">Dashboard 1</span>
                         </a>
                     </li>
                 </ul>
             </li>
             <li class="heading">
                 <h3 class="uppercase">Features</h3>
             </li>
             <li class="nav-item  ">
                 <a href="javascript:;" class="nav-link nav-toggle">
                     <i class="icon-diamond"></i>
                     <span class="title">Authentication</span>
                     <span class="arrow"></span>
                 </a>
                 <ul class="sub-menu">
                     <li class="nav-item  ">
                         <a href="" class="nav-link ">
                             <span class="title">Users</span>
                         </a>
                     </li>
                     <li class="nav-item  ">
                         <a href="" class="nav-link ">
                             <span class="title">Roles</span>
                         </a>
                     </li>
                     <li class="nav-item  ">
                         <a href="" class="nav-link ">
                             <span class="title">Permissions</span>
                         </a>
                     </li>

                 </ul>
             </li>
             <li class="nav-item  ">
                 <a href="javascript:;" class="nav-link nav-toggle">
                     <i class="icon-puzzle"></i>
                     <span class="title">Saleman</span>
                     <span class="arrow"></span>
                 </a>
                 <ul class="sub-menu">
                     <li class="nav-item  ">
                         <a href="components_noui_sliders.html" class="nav-link ">
                             <span class="title">Salesmen Accounts</span>
                         </a>
                     </li>
                     <li class="nav-item  ">
                         <a href="components_knob_dials.html" class="nav-link ">
                             <span class="title">Salesmen Categories</span>
                         </a>
                     </li>
                     <li class="nav-item  ">
                         <a href="components_knob_dials.html" class="nav-link ">
                             <span class="title">Salesmen Stock</span>
                         </a>
                     </li>
                     <li class="nav-item  ">
                         <a href="components_knob_dials.html" class="nav-link ">
                             <span class="title">Salesmen Sales</span>
                         </a>
                     </li>
                     <li class="nav-item  ">
                         <a href="components_knob_dials.html" class="nav-link ">
                             <span class="title">Salesmen Targets</span>
                         </a>
                     </li>
                     <li class="nav-item  ">
                         <a href="components_knob_dials.html" class="nav-link ">
                             <span class="title">Salesmen Visits</span>
                         </a>
                     </li>
                 </ul>
             </li>
             <li class="nav-item  ">
                 <a href="javascript:;" class="nav-link nav-toggle">
                     <i class="icon-settings"></i>
                     <span class="title">Point Of Sales</span>
                     <span class="arrow"></span>
                 </a>
                 <ul class="sub-menu">
                     <li class="nav-item  ">
                         <a href="form_controls.html" class="nav-link ">
                             <span class="title">Point Of Sales
                         </a>
                     </li>
                     <li class="nav-item  ">
                         <a href="form_controls_md.html" class="nav-link ">
                             <span class="title">Point Of Sales Categories
                         </a>
                     </li>
                 </ul>
             </li>
             <li class="nav-item  ">
                 <a href="javascript:;" class="nav-link nav-toggle">
                     <i class="icon-bulb"></i>
                     <span class="title">Stores</span>
                     <span class="arrow"></span>
                 </a>
                 <ul class="sub-menu">
                     <li class="nav-item  ">
                         <a href="elements_steps.html" class="nav-link ">
                             <span class="title">Stores</span>
                         </a>
                     </li>

                 </ul>
             </li>
             <li class="nav-item  ">
                 <a href="javascript:;" class="nav-link nav-toggle">
                     <i class="icon-briefcase"></i>
                     <span class="title">Purchases</span>
                     <span class="arrow"></span>
                 </a>
                 <ul class="sub-menu">
                     <li class="nav-item  ">
                         <a href="./main_store_sims" class="nav-link ">
                             <span class="title">Main Store sims</span>
                         </a>
                     </li>
                 </ul>
             </li>
             <li class="nav-item  ">
                 <a href="?p=" class="nav-link nav-toggle">
                     <i class="icon-wallet"></i>
                     <span class="title">Transfer</span>
                     <span class="arrow"></span>
                 </a>
                 <ul class="sub-menu">
                     <li class="nav-item  ">
                         <a href="portlet_boxed.html" class="nav-link ">
                             <span class="title">Branches</span>
                         </a>
                     </li>
                     <li class="nav-item  ">
                         <a href="portlet_light.html" class="nav-link ">
                             <span class="title">Salesmen</span>
                         </a>
                     </li>
                     <li class="nav-item  ">
                         <a href="portlet_solid.html" class="nav-link ">
                             <span class="title">Showrooms</span>
                         </a>
                     </li>
                     <li class="nav-item  ">
                         <a href="portlet_ajax.html" class="nav-link ">
                             <span class="title">Salespoint Store</span>
                         </a>
                     </li>
                 </ul>
             </li>
             <li class="nav-item  ">
                 <a href="javascript:;" class="nav-link nav-toggle">
                     <i class="icon-bar-chart"></i>
                     <span class="title">Sims</span>
                     <span class="arrow"></span>
                 </a>
                 <ul class="sub-menu">
                     <li class="nav-item  ">
                         <a href="charts_amcharts.html" class="nav-link ">
                             <span class="title">Virtual Sims</span>
                         </a>
                     </li>
                     <li class="nav-item  ">
                         <a href="charts_flotcharts.html" class="nav-link ">
                             <span class="title">Sims</span>
                         </a>
                     </li>
                     <li class="nav-item  ">
                         <a href="./sim_type" class="nav-link ">
                             <span class="title">Sim Types</span>
                         </a>
                     </li>
                 </ul>
             </li>
             <li class="nav-item  ">
                 <a href="javascript:;" class="nav-link nav-toggle">
                     <i class="icon-pointer"></i>
                     <span class="title">Sales Orders</span>
                     <span class="arrow"></span>
                 </a>
                 <ul class="sub-menu">
                     <li class="nav-item  ">
                         <a href="maps_google.html" class="nav-link ">
                             <span class="title">Sales Orders</span>
                         </a>
                     </li>
                 </ul>
             </li>


             <li class="nav-item  ">
                 <a href="javascript:;" class="nav-link nav-toggle">
                     <i class="icon-pointer"></i>
                     <span class="title">Lookup Tables</span>
                     <span class="arrow"></span>
                 </a>
                 <ul class="sub-menu">
                     <li class="nav-item  ">
                         <a href="maps_google.html" class="nav-link ">
                             <span class="title">Nationalities</span>
                         </a>
                     </li>
                     <li class="nav-item  ">
                         <a href="maps_google.html" class="nav-link ">
                             <span class="title">Departments</span>
                         </a>
                     </li>
                     <li class="nav-item  ">
                         <a href="maps_google.html" class="nav-link ">
                             <span class="title">Branches</span>
                         </a>
                     </li>
                     <li class="nav-item  ">
                         <a href="maps_google.html" class="nav-link ">
                             <span class="title">Cities</span>
                         </a>
                     </li>
                     <li class="nav-item  ">
                         <a href="maps_google.html" class="nav-link ">
                             <span class="title">Sectors</span>
                         </a>
                     </li>
                     <li class="nav-item  ">
                         <a href="maps_google.html" class="nav-link ">
                             <span class="title">Zones</span>
                         </a>
                     </li>
                 </ul>

             </li>







         </ul>

     </div>
     <!-- END SIDEBAR -->
 </div>