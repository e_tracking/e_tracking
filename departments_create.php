<?php ob_start(); ?>
<?php require_once './pdo_connection.php' ?>
<?php include 'header.php'; ?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="index.html">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Lookup Tables</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Departments create</span>
        </li>
    </ul>
</div>

<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Add Department
                </div>
            </div>

            <div class="portlet-body form">
                <?php 
                if(isset($_GET['status']))
                if($_GET['status'] == 'suc')
                {?>
                <div class="alert alert-success" role="alert">
                    data successfully saved!
                </div>
                <?php } else { ?>
                <div class="alert alert-danger" role="alert">
                    there is a problem with saving data!
                </div>
                <?php } ?>
                <!-- BEGIN FORM-->
                <form action="" method="post">

                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Add Department *
                            </label>
                            <input type="text" name="dep_name" class="form-control" placeholder="Enter name" required>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="btn-set pull-left">
                            <button type="submit" name="insert" class="btn green">Add</button>
                        </div>

                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
    </div>
</div>

</div>
</div>
<?php include 'footer.php'; ?>
<?php
if(isset($_POST['insert']))
{
    if (empty($_POST['dep_name'])) {
        header("Location:  departments_create?status=failed");

}

  $sql = "INSERT INTO `departments` (`dep_name`) VALUES (?)";
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(1, $_POST['dep_name'], PDO::PARAM_STR);
  if($stmt->execute())
  {
    header("Location: departments_create?status=suc");
  }
  else
  {
    header("Location:  departments_create?status=failed");
  }
}


?>