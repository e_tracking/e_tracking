<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.6
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title>Metronic | Select2 Dropdowns</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
        type="text/css" />
    <link href="./assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="./assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="./assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="./assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet"
        type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="./assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="./assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="./assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="./assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="./assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="./assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css"
        id="style_color" />
    <link href="./assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <div class="page-wrapper">
        <!-- BEGIN HEADER -->

        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->

        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">

                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">

                    <h1 class="page-title"> Buttons Datatable
                        <small>buttons extension demos</small>
                    </h1>
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase">Buttons</span>
                                    </div>
                                </div>
                                <div class="form-group">
            <label for="single" class="control-label">Select2 single select</label>
            <select id="single" class="form-control select2">
                <option></option>
                <optgroup label="Alaskan">
                    <option value="AK">Alaska</option>
                </optgroup>
         
        
           
            </select>
        </div>
<br><br><br><br><br>

                                <table id="example" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>user_login</th>
                                            <th>user_nicename</th>
                                            <th>user_email</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>user_login</th>
                                            <th>user_nicename</th>
                                            <th>user_email</th>
                                        </tr>
                                        <tr>
                                            <th>user_login</th>
                                            <th>user_nicename</th>
                                            <th>user_email</th>
                                        </tr>
                                        <tr>
                                            <th>user_login</th>
                                            <th>user_nicename</th>
                                            <th>user_email</th>
                                        </tr>
                                        <tr>
                                            <th>user_login</th>
                                            <th>user_nicename</th>
                                            <th>user_email</th>
                                        </tr>
                                        <tr>
                                            <th>user_login</th>
                                            <th>user_nicename</th>
                                            <th>user_email</th>
                                        </tr>
                                        <tr>
                                            <th>user_login</th>
                                            <th>user_nicename</th>
                                            <th>user_email</th>
                                        </tr>
                                        <tr>
                                            <th>user_login</th>
                                            <th>user_nicename</th>
                                            <th>user_email</th>
                                        </tr>
                                        <tr>
                                            <th>user_login</th>
                                            <th>user_nicename</th>
                                            <th>user_email</th>
                                        </tr>
                                        <tr>
                                            <th>user_login</th>
                                            <th>user_nicename</th>
                                            <th>user_email</th>
                                        </tr>
                                        <tr>
                                            <th>user_login</th>
                                            <th>user_nicename</th>
                                            <th>user_email</th>
                                        </tr>
                                        <tr>
                                            <th>user_login</th>
                                            <th>user_nicename</th>
                                            <th>user_email</th>
                                        </tr>
                                        <tr>
                                            <th>user_login</th>
                                            <th>user_nicename</th>
                                            <th>user_email</th>
                                        </tr>
                                        <tr>
                                            <th>user_login</th>
                                            <th>user_nicename</th>
                                            <th>user_email</th>
                                        </tr>
                                        <tr>
                                            <th>user_login</th>
                                            <th>user_nicename</th>
                                            <th>user_email</th>
                                        </tr>
                                        <tr>
                                            <th>user_login</th>
                                            <th>user_nicename</th>
                                            <th>user_email</th>
                                        </tr>
                                        <tr>
                                            <th>user_login</th>
                                            <th>user_nicename</th>
                                            <th>user_email</th>
                                        </tr>
                                        <tr>
                                            <th>user_login</th>
                                            <th>user_nicename</th>
                                            <th>user_email</th>
                                        </tr>
                                        <tr>
                                            <th>user_login</th>
                                            <th>user_nicename</th>
                                            <th>user_email</th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>




            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>

    </div>
    <!--[if lt IE 9]>
<script src="./assets/global/plugins/respond.min.js"></script>
<script src="./assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>

    <!-- BEGIN CORE PLUGINS -->
    <script src="./assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="./assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="./assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="./assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="./assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="./assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="./assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="./assets/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="./assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="./assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
    <script src="./assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
    <script src="./assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
    <!-- END THEME LAYOUT SCRIPTS -->


    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap.min.js"></script>


    <script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
    <script>
    $(document).ready(function() {
        var url = "data.php?id=";
        $('#button').on('click', function(e) {

            var id = document.getElementById('id').value;
            url = "data?id=" + id;
            myFunction(url);
        });

        var table = $('#example').dataTable({
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            "pageLength": 10,



        });
    });
    </script>
    <!-- END THEME LAYOUT SCRIPTS -->
</body>