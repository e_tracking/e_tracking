<?php require_once 'pdo_connection.php'; ?>
<?php require_once 'header.php'; ?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="index.html">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Porchase</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Main store</span>
        </li>
    </ul>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Main Sims
                    </span>

                </div>
                <div class="tools">
                    <button type="button" onclick="location.href='./import_purchase'"
                        class="btn blue mt-ladda-btn ladda-button btn-circle" data-style="slide-right">
                        <span class="ladda-label">Import Purchase order file</span>
                        <span class="ladda-spinner"></span>
                    </button>
                    <button type="button" onclick="location.href=''"
                        class="btn blue mt-ladda-btn ladda-button btn-circle" data-style="slide-right">
                        <span class="ladda-label">Purchase return file</span>
                        <span class="ladda-spinner"></span>
                    </button>
                </div>
                <br> <br> <br>
                <form action="" method="POST">
                    <div class="pull-left">
                        <div class="form-group" style="float: left;margin-right: 2.5em;">
                            <label class="control-label">Sim Types</label>
                            <select class="form-control" name="sim_types">
                                <option selected disabled>Select...</option>
                                <?php
                     $sql = "SELECT * FROM `sim_types` GROUP by sim_name";
                     $stmt = $conn->prepare($sql);         
                     $stmt->execute();
                   
                       while ($row = $stmt->fetch())
                       {  ?>
                                <option value="<?php echo $row[1];?>"><?php echo $row[1];?></option>

                                <?php } ?>
                            </select>
                        </div>


                        <div class="form-group" style="float: left;margin-right: 2.5em;">
                            <label class="control-label">Cities</label>
                            <select class="form-control" name="city">
                                <option selected disabled>Select...</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                        </div>
                        <div class="form-group" style="float: left;margin-right: 2.5em;">
                            <label class="control-label">From</label>
                            <input type="date" value="" name="from" class="form-control">
                        </div>
                        <div class="form-group" style="float: left;margin-right: 2.5em;">
                            <label class="control-label">To</label>
                            <input type="date" value="" name="to" class="form-control">
                        </div>


                        <div class="form-actions">
                            <div class="btn-set">

                                <button type="submit" name="search" class="btn red"> <i
                                        class="fa fa-search"></i>&nbsp;Search</button>
                                <br> <br>
                            </div>

                        </div>


                    </div>
                </form>
            </div>
            <br><br><br><br><br>
            <table class="table table-striped table-bordered table-hover" id="sample_1">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Number</th>
                        <th>File Name</th>
                        <th>Purchase Date</th>
                        <th>Supplying Number</th>
                        <th>Supplying Date</th>
                        <th>ERP Number</th>
                        <th>City</th>
                        <th>From Store</th>
                        <th>To Store</th>
                        <th>Type</th>
                        <th>Sim Type</th>
                        <th>Quantity</th>
                        <th>File</th>
                        <th>Note</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                 $sql = "SELECT * FROM purchase_to_mainstore";
                if($_POST)
                {

                    //part 1
                    $from = $_POST['from'];
                    $from_len = strlen($from);

                    $to = $_POST['to'];
                    $to_len = strlen($to);

                    if(isset($_POST['sim_types']))
                    {
                        $sim_types = $_POST['sim_types'];
                        $sql = "SELECT * FROM purchase_to_mainstore where sim_types = '$sim_types'";
                    }
                    if(isset($_POST['sim_types']) && isset($_POST['city']))
                    {
                        $sim_types = $_POST['sim_types'];
                        $city = $_POST['city'];
                        $sql = "SELECT * FROM purchase_to_mainstore where sim_types = '$sim_types' and city = '$city'";
                    }
                    if(isset($_POST['sim_types']) && isset($_POST['city']) && $from_len > 1)
                    {
                        $sim_types = $_POST['sim_types'];
                        $city = $_POST['city'];
                        $from = $_POST['from'];
                        $sql = "SELECT * FROM purchase_to_mainstore where sim_types = '$sim_types' and city = '$city' and purchase_date = '$from'";
                    }

                    if(isset($_POST['sim_types']) && isset($_POST['city']) && $from_len > 1 && $to_len > 1)
                    {
                        $sim_types = $_POST['sim_types'];
                        $city = $_POST['city'];
                        $from = $_POST['from'];
                        $to = $_POST['to'];
                        $sql = "SELECT * FROM purchase_to_mainstore where sim_types = '$sim_types' and city = '$city' and purchase_date between '$from' and '$to'";
                    }
                    //
                    //part 2
                    if($from_len > 1 && $to_len > 1)
                    {
                        $from = $_POST['from'];
                        $to = $_POST['to'];
                        $sql = "SELECT * FROM purchase_to_mainstore where purchase_date between '$from' and '$to'";
                    }
                    if($from_len > 1)
                    {
                        $from = $_POST['from'];
                        $sql = "SELECT * FROM purchase_to_mainstore where purchase_date = '$from'";
                    }
                    //


                    //part 3
                    if(isset($_POST['city']) && $from_len > 1 && $to_len > 1)
                    {
                        $city = $_POST['city'];
                        $from = $_POST['from'];
                        $to = $_POST['to'];
                        $sql = "SELECT * FROM purchase_to_mainstore where  city = '$city' and purchase_date between '$from' and '$to'";
                    }

                }
                ?>

                    <?php
                     $stmt = $conn->prepare($sql);         
                     $stmt->execute();
                     $i = 1;
                       while ($row = $stmt->fetch())
                       {
                       
                    ?>
                    <tr>
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $row[0]; ?></td>
                        <td><?php echo $row[1]; ?></td>
                        <td><?php echo $row[2]; ?></td>
                        <td><?php echo $row[3]; ?></td>
                        <td><?php echo $row[4]; ?></td>
                        <td><?php echo $row[5]; ?></td>
                        <td><?php echo $row[6]; ?></td>
                        <td><?php echo $row[10]; ?></td>
                        <td><?php echo $row[11]; ?></td>
                        <td><?php echo $row[12]; ?></td>
                        <td><?php echo $row[7]; ?></td>
                        <td><?php echo $row[8]; ?></td>
                        <td><a href="./<?php echo $row[13]; ?>">Open File</a></td>
                        <td><?php echo $row[9]; ?></td>

                    </tr>
                    <?php } ?>


                </tbody>
            </table>




            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
</div>

</div>
</div>



<?php require_once 'footer.php'; ?>