<?php ob_start(); ?>
<?php require_once './pdo_connection.php' ?>
<?php include 'header.php'; ?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="index.html">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Sims</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Add Sim Type</span>
        </li>
    </ul>
</div>

<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Add Sim Type
                </div>
            </div>

            <div class="portlet-body form">
                <?php 
                if(isset($_GET['status']))
                if($_GET['status'] == 'suc')
                {?>
                <div class="alert alert-success" role="alert">
                    data successfully saved!
                </div>
                <?php } else { ?>
                <div class="alert alert-danger" role="alert">
                    there is a problem with saving data!
                </div>
                <?php } ?>
                <!-- BEGIN FORM-->
                <form action="" method="post">

                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Add Sim Type *
                            </label>
                            <input type="text" name="sim_name" class="form-control" placeholder="Enter name" required>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="btn-set pull-left">
                            <button type="submit" name="insert" class="btn green">Add</button>
                        </div>

                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
    </div>
</div>

</div>
</div>
<?php include 'footer.php'; ?>
<?php
if(isset($_POST['insert']))
{
    if (empty($_POST['sim_name'])) {
        header("Location:  sim_type_create.php?status=failed");

}

  $sql = "INSERT INTO `sim_types`(`sim_name`) VALUES (?)";
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(1, $_POST['sim_name'], PDO::PARAM_STR);
  if($stmt->execute())
  {
    header("Location: sim_type_create.php?status=suc");
  }
  else
  {
    header("Location:  sim_type_create.php?status=failed");
  }
}


?>